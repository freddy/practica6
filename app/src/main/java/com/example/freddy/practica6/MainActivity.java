package com.example.freddy.practica6;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button ingresar;
    Button foto;
    Button sensores;
    Button salir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ingresar = (Button)findViewById(R.id.ingresar);

        ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ingresar = new Intent( MainActivity.this, actividad_Ingresar.class);
                startActivity(ingresar);

            }
        });

        sensores = (Button) findViewById(R.id.sensores);

        sensores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sesonres =new Intent(MainActivity.this, ActividadSensorAcelerometro.class);
                startActivity(sesonres);
            }
        });
    }
}
